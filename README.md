


# Template Laravel de Dashboard Material Design 2019 - Gabriel Marczuk Thá

![Materialize](https://scotch-res.cloudinary.com/image/upload/w_1050,q_auto:good,f_auto/media/https://scotch.io/wp-content/uploads/2015/01/getting-started-materialize-css-framework.png)

> Esse template de projeto utiliza a biblioteca de Material Design chamada Materialize  
> Para saber mais sobre a biblioteca acesse: [Materialize](https://materializecss.com/)    
> Para acessar a documentação do framework Laravel acesse: [Laravel](https://laravel.com/)  



## Como usar:

Primeiro, é necessário clonar o projeto:  
`git clone https://gitlab.com/Gabriel_Tha/dashboard-2019.git`  

Após isso, iremos configurar o laravel:  
`cd dashboard-2019`  
`composer install`  

E então, podemos começar a utilizar o projeto:  
`php artisan serve`

Entre em seu navegador e utilize a URL:   
**localhost:8000/materialize**

A base do template está em: **/resources/views/appMaterialize.blade.php**  
A pagina inicial está em: **/resources/views/materialize.blade.php**

### Voilà

Agora, pegue os elementos da pagina da biblioteca [Materialize](https://materializecss.com/) e adicione as paginas 

---
> Links interessantes:
>* [PADRÃO DE NOMENCLATURA CSS](https://tableless.com.br/oocss-smacss-bem-dry-css-afinal-como-escrever-css/)  
>* [GIT WORKFLOW](https://medium.com/trainingcenter/utilizando-o-fluxo-git-flow-e63d5e0d5e04)



